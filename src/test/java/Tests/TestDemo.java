package Tests;

import io.restassured.response.ResponseBody;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;

public class TestDemo {
     @Test
    public void MyTestAPI(){
       given().
                when().
                get("http://ergast.com/api/f1/2017/circuits.json").
                then().
                statusCode(200).
                assertThat().
                body("MRData.CircuitTable.Circuits.circuitId",hasSize(20));
        // given().body("hullo").expect().statusCode(200).when().get("/getWithContent");
    }

    @Test
    public void MyTestAPI2(){
        given().
                when().
                get("https://localramu.com/api/services/get").
                then().
                statusCode(200)
               .contentType("application/json");
        // given().body("hullo").expect().statusCode(200).when().get("/getWithContent");
    }



}

